var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var jshint = require('gulp-jshint');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var merge = require('merge-stream');

var paths = {
    sass: ['./scss/**/*.scss'],
    dist: './../training-app-distribution/'
};

gulp.task('default', ['sass']);

gulp.task('lint', function() {
  return gulp.src('./www/app/**/*.js')
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

gulp.task('usemin', function() {
  var js = gulp.src('./www/*.html')
      .pipe(usemin({
          css: [ ],
          html: [ function () {return htmlmin({ collapseWhitespace: true });} ],
          js: [ uglify ],
          inlinejs: [ uglify ],
          inlinecss: [ 'concat' ]
      }))
      .pipe(gulp.dest(paths.dist + 'www/'));

   var html = gulp.src('./www/app/views/*.html', {base: './www/'})
      .pipe(gulp.dest(paths.dist + 'www/'));

    var data = gulp.src('./www/app/*.json')
      .pipe(gulp.dest(paths.dist + 'www/app/'));

    var fonts = gulp.src('./www/lib/ionic/fonts/*.*')
      .pipe(gulp.dest(paths.dist + 'www/fonts/'));

    var images = gulp.src('./www/img/**', {base: './www/'})
      .pipe(gulp.dest(paths.dist + 'www/'));

    var translations = gulp.src('./www/translations/*.*')
      .pipe(gulp.dest(paths.dist + 'www/translations/'));

    var platforms = gulp.src('./platforms/**')
        .pipe(gulp.dest(paths.dist + 'platforms'));

    var plugins = gulp.src('./plugins/**')
        .pipe(gulp.dest(paths.dist + 'plugins'));

    var resources = gulp.src('./resources/**')
        .pipe(gulp.dest(paths.dist + 'resources'));

    var hooks = gulp.src('./hooks/**')
        .pipe(gulp.dest(paths.dist + 'hooks'));

    var config = gulp.src('./config.xml')
        .pipe(gulp.dest(paths.dist));

    return merge([js, html, data, fonts, images, translations, platforms, plugins, resources, hooks, config]);
}); 