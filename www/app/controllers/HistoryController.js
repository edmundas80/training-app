angular.module('training-app.controllers')
    .controller('HistoryController', ['$scope', '$log', '$q', '$state', 'ActivityHistoryService', 'DateTimeService', function($scope, $log, $q, $state, activityHistoryService, dateTimeService) {
        var self = this;

        $scope.lastId = null;
        $scope.historyItems = [];
        $scope.hasMore = true;

        $scope.$on("$ionicView.loaded", function(){
            self.init()
                .then(function(){
                    $log.log($scope.historyItems);
                })
                .catch(function(error){
                    $log.error(error);
                });
        });

        $scope.loadData = function(){
            if (!$scope.hasMore){
                $scope.$broadcast('scroll.infiniteScrollComplete');
                return $q.when([]);
            }

            return activityHistoryService.getList($scope.lastId)
                .then(function(result){
                    $scope.lastId = result.lastId;

                    var lastItems = result.docs.map(function(historyItem){
                        var trainingDateFormatted = dateTimeService.toLongDate(historyItem.historyDate);
                        var duration = dateTimeService.toDuration(historyItem.duration);

                        return  {date: trainingDateFormatted, exerciseCount: historyItem.exerciseCount, duration: duration, id: historyItem._id};
                    });

                    $scope.historyItems = [].concat($scope.historyItems, lastItems);
                    $scope.hasMore = lastItems.length > 0;
                })
                .then(function(result){
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    return result;
                });
        };

        $scope.goToDetails = function(id){
            $state.go('tab.history-details', { id: id })
        }

        self.init = function(){
            return $scope.loadData()
                .then(function(){
                    $log.log($scope.historyItems);
                })
                .catch(function(error){
                    $log.error(error);
                });
        };
    }]);