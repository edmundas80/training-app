describe('ExerciseListController', function() {
    beforeEach(function() {
        module(function($provide){
            $provide.constant("DB_NAME", "TESTDB");
        });

        module('pouchdb');
        module('training-app.services');
        module('training-app.controllers');
    });

    var controller;
    var $scope;
    var $q;
    var $rootScope;
    var $state;
    var ExerciseService;

    beforeEach(inject(function(_$controller_, _$rootScope_, _$log_, _ExerciseService_, _$q_){
        $scope = _$rootScope_.$new();
        $q = _$q_;
        $rootScope = _$rootScope_;
        $state = { go: function(){} };
        ExerciseService = _ExerciseService_;
        controller = _$controller_('ExerciseListController', { $scope: $scope, $rootScope: _$rootScope_, $stateParams: {level: 1}, $log: _$log_, $state: $state, ExerciseService: _ExerciseService_});
    }));

    describe('$scope.save', function() {
        it('save successfully', function(done) {
            spyOn(controller, "saveDraft").and.returnValue($q.when());
            spyOn(ExerciseService, "getExerciseConfigurationDraft");
            spyOn(ExerciseService, "saveExerciseSummaryFromDraft");
            spyOn($state, "go");

            $scope.save()
                .then(function(){
                    expect(controller.saveDraft).toHaveBeenCalled();
                    expect(ExerciseService.getExerciseConfigurationDraft).toHaveBeenCalled();
                    expect(ExerciseService.saveExerciseSummaryFromDraft).toHaveBeenCalled();
                    expect($state.go).toHaveBeenCalledWith("tab.exercises-summary");
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        });
    });
});