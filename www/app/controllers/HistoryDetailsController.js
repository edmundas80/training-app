angular.module('training-app.controllers')
    .controller('HistoryDetailsController', ['$scope', '$log', '$q', '$stateParams', 'ActivityHistoryService', 'ExerciseService', 'DateTimeService', 'SUCCESS_RATES', function($scope, $log, $q, $stateParams, activityHistoryService, exerciseService, dateTimeService, SUCCESS_RATES) {
        var self = this;

        $scope.id = null;
        $scope.successRates = SUCCESS_RATES;
        $scope.history = null;

        $scope.$on("$ionicView.loaded", function(){
            $scope.id = $stateParams.id;
            $scope.variations = [];

            self.init()
                .then(function(){
                    return exerciseService.getExerciseVariations()
                        .then(function(variations){
                            $scope.variations = variations;
                        });
                })
                .catch(function(error){
                    $log.error(error);
                });
        });

        $scope.loadData = function(){
            return activityHistoryService.get($scope.id)
                .then(function(result){
                    var trainingDateFormatted = dateTimeService.toLongDate(result.historyDate);
                    var duration = dateTimeService.toDuration(result.duration);

                    $scope.history =  {date: trainingDateFormatted, exerciseCount: result.exerciseCount, duration: duration, id: result._id, completedExercises: []};

                    angular.forEach(result.completedExercises, function(exercise){
                        var singleExerciseDuration = dateTimeService.toDuration(exercise.duration);
                        var historyExercise = {id: exercise._id, duration: singleExerciseDuration, rating: $scope.successRates[exercise.rating.rating1][exercise.rating.rating2]};
                        $scope.history.completedExercises.push(historyExercise);
                    });

                    return result;
                });
        };

        self.init = function(){
            return $scope.loadData()
                .then(function(){
                    $log.log($scope.history);
                });
        };
    }]);