angular.module('training-app.controllers')
    .controller('ExerciseListController', ['$scope', '$rootScope', '$stateParams', '$log', '$state', 'ExerciseService', function($scope, $rootScope, $stateParams, $log, $state, exerciseService) {
        var self = this;

        $scope.level = $stateParams.level;
        $scope.forms = {};
        $scope.items = [];
        $scope.dirty = false;
        $scope.storedVariations = [];

        $scope.$watch('forms.exerciseForm.$dirty', function(newVal){
            if (newVal){
                $scope.dirty = newVal;
            }
        });

        $scope.$on("$ionicView.loaded", function(){
            self.init()
                .catch(function(error){
                    $log.error(error);
                });;
        });

        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });

        var stateChangeListener = $rootScope.$on('$stateChangeStart', function(event, toState, toParams){
            self.navigateToState(event, toState, toParams);
            stateChangeListener();
        });

        $scope.save = function(){
            $log.log("Save started");

            return self.saveDraft()
                .then(exerciseService.getExerciseConfigurationDraft)
                .then(exerciseService.saveExerciseSummaryFromDraft)
                .then(function(){
                    return $state.go("tab.exercises-summary");
                })
                .catch(function(error){
                    $log.error(error);
                });
        };

        $scope.discard = function(){
            $log.log("Discard started");
            return $state.go("tab.exercises-summary");
        };

        self.init = function(){
            $log.log("Configuration draft view initializing.");

            return exerciseService.getExerciseConfigurationDraft($scope.level)
                .then(function(doc){
                    $log.log("Draft view configuration reading.");
                    $scope.dirty = doc.dirty;
                    $scope.storedVariations = doc.level[$scope.level - 1];
                })
                .then(function(){
                    return exerciseService.getExercises($scope.level);
                })
                .then(function(exercises){
                    angular.forEach(exercises, function(exercise){
                        return angular.forEach(exercise.variations, function(variation){
                            variation.selected = $scope.storedVariations.indexOf(variation._id) >= 0;
                            return variation;
                        });
                    });

                    $log.log("Exercises loaded from DB");
                    $scope.items = exercises;
                });
        };

        self.saveDraft = function(){
            $log.log("Draft save initiated.");

            var selectedVariations = [];

            angular.forEach($scope.items, function(exercise){
                angular.forEach(exercise.variations, function(variation){
                    if (variation.selected){
                        selectedVariations.push(variation);
                    }
                });
            });

            return exerciseService.saveExerciseConfigurationDraft(selectedVariations, $scope.level)
                .then(function(){
                    $scope.dirty = false;
                    $log.log("Draft saved successfully.");
                })
                .catch(function(error){
                    $log.error("Saving configuration failed. ", error);
                });
        };

        self.navigateToState = function(event, toState, toParams){
            $log.log("Navigating to state: ", toState.name);

            if (toState.name != "tab.exercises.level-1" && toState.name != "tab.exercises.level-2" && toState.name != "tab.exercises.level-3"){
                exerciseService.removeExerciseConfigurationDraft()
                    .then(function(){
                        $log.log("Configuration draft deleted. ");
                    })
                    .catch(function(error){
                        $log.error("Configuration draft not deleted. ", error);
                    });
            } else if ($scope.dirty){
                $log.log("Preventing state transition");

                event.preventDefault();

                self.saveDraft()
                    .then(function(){
                        $state.go(toState, toParams);
                    })
                    .catch(function(error){
                        $log.error(error);
                    });

            } else {
                $log.log("Continuing with state transition");
            }
        }
    }]);
