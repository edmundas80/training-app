angular.module('training-app.controllers')
    .controller('ExercisesSummaryController', ['$scope', '$state', '$log', 'ExerciseService', function($scope, $state, $log, exerciseService) {
        var self = this;

        $scope.showDelete = false;
        $scope.showReorder = false;
        $scope.hasData = false;
        $scope.initialized = false;
        $scope.items = [];
        $scope.variations = [];

        $scope.addRemoveExercises = function(){
            $state.go("tab.exercises");
        };

        $scope.$on("$ionicView.loaded", function(){
            self.init(true);
        });

        $scope.toggleDelete = function(){
            $scope.showDelete = !$scope.showDelete;
        };

        $scope.toggleReorder = function(){
            $scope.showReorder = !$scope.showReorder;
        };

        $scope.saveChanges = function(){
            exerciseService.saveExerciseSummaryExercises($scope.items)
                .then(self.init)
                .then(self.resetActionButtonVisibility)
                .catch(function(error){
                    $log.error(error);
                });
        };

        $scope.cancelChanges = function(){
            self.init()
                .then(self.resetActionButtonVisibility)
                .catch(function(error){
                    $log.error(error);
                });
        };

        $scope.moveItem = function(item, fromIndex, toIndex) {
            $scope.items.splice(fromIndex, 1);
            $scope.items.splice(toIndex, 0, item);
        };

        $scope.onItemDelete = function(item) {
            $scope.items.splice($scope.items.indexOf(item), 1);
        };

        self.init = function(loadVariations){
            return exerciseService.getExerciseSummary()
                .then(function(summary){
                    $scope.items = summary.exercises;
                    $scope.hasData = $scope.items > [];
                    $scope.initialized = true;
                })
                .then(function(){
                    if (loadVariations){
                        exerciseService.getExerciseVariations()
                            .then(function(variations){
                                $scope.variations = variations;
                            });
                    }
                });
        };

        self.resetActionButtonVisibility = function() {
            $scope.showReorder = false;
            $scope.showDelete = false;
            $scope.hasData = $scope.items > [];
        };
    }]);