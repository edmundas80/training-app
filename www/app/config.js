angular.module('training-app')
    .constant('moment', moment)
    .constant('locale', "lt")
    .constant('DB_NAME', 'training-db')
    .constant('SUCCESS_RATES', [
        ['cry.png', 'agressive.png', 'grimace.png'],
        ['cool.png', 'cant-believe-it.png', 'wink.png'],
        ['gape.png', 'glad.png', 'lol.png'],
        ['cheer.png', 'smile.png', 'muhaha.png']
    ]);