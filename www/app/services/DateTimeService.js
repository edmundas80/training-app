angular.module('training-app.services')
    .service('DateTimeService', ['moment', 'locale', function(moment, locale){
        var self = this;

        self.toLongDate = function(unixDate){
            moment.locale(locale);

            var trainingDate = moment.unix(unixDate);
            return trainingDate.format("LLL");
        };

        self.toDuration = function(durationInSeconds){
            moment.locale(locale);

            return moment.duration(durationInSeconds, "seconds").humanize();
        };
    }]);