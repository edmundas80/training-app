angular.module('training-app.services')
    .factory('Repository', function(pouchDB, DB_NAME, $q){
        var db = pouchDB(DB_NAME);
        var repository = {};
        repository.db = db;

        repository.all = function(options){
            options = options || {};
            var o = {include_docs: true};

            if (options.startKey){
                o.startkey = options.startKey;
            }

            if (options.endKey){
                o.endkey = options.endKey;
            }

            if (options.pageSize){
                o.limit = options.pageSize;
            }

            if (options.skip){
                o.skip = options.skip;
            }

            if (options.descending){
                o.descending = options.descending;
            }

            return db.allDocs(o)
                .then(function(docs){
                    var found = docs.rows.map(function(value){
                        return value.doc;
                    });

                    var last = (docs.rows && docs.rows.length > 0) ? docs.rows[docs.rows.length - 1].doc._id : null;

                    return { docs: found, lastId: last };
                });
        };

        repository.get = function(id){
            if (!id){
                return $q.reject("id is not specified");
            }

            return db.get(id);
        };

        repository.getSafe = function(id, createDocFn){
            if (!id){
                return $q.reject("id is not specified");
            }

            if (!createDocFn){
                return $q.reject("createDocFn is not specified");
            }

            return repository.exists(id)
                .then(function(exists){
                    if (exists){
                        return db.get(id);
                    }

                    return createDocFn();
                });
        };

        repository.exists = function(id){
            if (!id){
                return $q.reject("id is not specified");
            }

            return db.get(id)
                .then(function(doc){
                    return !!doc;
                })
                .catch(function(error){
                    if (error && error.error && error.message == 'missing'){
                        return false;
                    }

                    throw error;
                });
        };

        repository.saveOrUpdate = function(doc){
            return $q.when(doc)
                .then(function(doc){
                    if (!doc || !doc._id) {
                        return $q.reject("No document with _id");
                    }

                    return doc;
                })
                .then(function(doc){
                    return db.get(doc._id)
                        .then(function(docFromDb){
                            doc._rev = docFromDb._rev;
                            return db.put(doc);
                        });
                })
                .catch(function(error){
                    if (error && error.error && error.message == 'missing'){
                        return db.put(doc);
                    }

                    throw error;
                });
        };

        repository.remove = function(id){
            if (!id){
                return $q.reject("id is not specified");
            }

            return repository.get(id)
                .then(function(doc){
                    return db.remove(doc);
                });
        };

        return repository;
    });
