angular.module('training-app.services')
    .constant('DB_VERSION_DOC_ID', '5e60ddd0-fdfe-4f1f-b6e8-9a344dcde9ef')
    .service('DatabaseMigration', ['Repository', 'DB_VERSION_DOC_ID', '$http', '$q', '$log', function(repository, dbVersionDocId, $http, $q, $log){
        var db = repository.db;

        this.executeDbMigration = function(){
            return this._getDbVersion()
                .then(this._updateDb);
        };

        this._getDbVersion = function(){
            return db.get(dbVersionDocId)
                .then(function(doc){
                    return 0;
                    //return doc.version;
                })
                .catch(function(error){
                    if (error && error.error && error.message == 'missing'){
                        return 0;
                    }

                    throw error;
                });
        };

        this._updateDb = function(dbVersion){
            return $http.get('app/data.json')
                .then(function(res){
                    var appDocuments = res.data;
                    var needsUpdate;

                    angular.forEach(appDocuments, function(doc){
                        if (doc._id == dbVersionDocId && doc.version > dbVersion){
                            needsUpdate = true;
                        }
                    });

                    if (needsUpdate){
                        return $q.when((function(){
                            return appDocuments.map(function(appDocument){
                                return appDocument._id;
                            });
                        })())
                        .then(function(ids){
                            return db.allDocs({keys: ids});
                        })
                        .then(function(docsToDelete){
                            if (docsToDelete.rows > []){
                                return $q.all(docsToDelete.rows.map(function(doc){
                                    if (!doc.error && !doc.value.deleted){
                                        return db.remove(doc.id, doc.value.rev);
                                    }
                                }));
                            }
                        })
                        .then(function(){
                            return db.bulkDocs(appDocuments);
                        });
                    }
                });
        };
    }]);