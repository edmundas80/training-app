angular.module('training-app.services')
    .constant("MIN_HISTORY_ID", "activityHistory_1")
    .constant("MAX_HISTORY_ID", "activityHistory_9999999999")
    .constant("PAGE_SIZE", 10)
    .service('ActivityHistoryService', ['Repository', 'ExerciseService', 'ActivityService', '$q', 'filterFilter', '$filter', 'moment', '$log', 'MIN_HISTORY_ID', 'MAX_HISTORY_ID', 'PAGE_SIZE', function(repository, exerciseService, activityService, $q, filterFilter, $filter, moment, $log, MIN_HISTORY_ID, MAX_HISTORY_ID, PAGE_SIZE){
        var self = this;

        self.saveActivityHistory = function(activity){
            $log.log("ActivityHistoryService.saveActivityHistory");

            if (!activity){
                return $q.reject("no activity set");
            }

            if (activity.completedExercises.length == 0) {
                return $q.reject("no completed exercises");
            }

            var historyId = "activityHistory_" + moment().unix();
            var history = self._createActivityHistory(historyId);

            history.historyDate = moment().unix();
            history.exerciseCount = activity.completedExercises.length;
            history.completedExercises = activity.completedExercises;

            angular.forEach(activity.completedExercises, function(exercise){
                history.duration += exercise.duration;
            });

            return repository.saveOrUpdate(history)
                .then(function(){
                    $log.log(history);
                    return history;
                })
        };

        self.getList = function(lastId){
            lastId = lastId || MAX_HISTORY_ID;

            var options = {startKey: lastId, pageSize: PAGE_SIZE, descending: true};

            if (lastId != MAX_HISTORY_ID){
                options.skip = 1;
            }

            return repository.all(options)
                .then(function(result){
                    var filter = {type: 'activityHistory'};
                    var historyItems = filterFilter(result.docs, filter);
                    var lastId = (historyItems.length > 0) ? historyItems[historyItems.length - 1]._id : null;
                    var r = {docs: historyItems, lastId: lastId};

                    $log.log(r);
                    return r;
                })
        };

        self.get = function(id){
            if (!id){
                return $q.reject("no id set");
            }

            return repository.get(id);
        };

        self._createActivityHistory = function(id){
            return {
                _id: id,
                type: "activityHistory",
                duration: 0,
                exerciseCount: 0,
                historyDate: null,
                completedExercises: []
            };
        };
    }]);