angular.module('training-app.services')
    .constant('ACTIVITY_DOC_ID', '60e5d7ef-e128-437e-8eab-f9a989a38caa')
    .service('ActivityService', ['Repository', 'ExerciseService', '$q', 'filterFilter', '$filter', 'ACTIVITY_DOC_ID', 'moment', '$log', function(repository, exerciseService, $q, filterFilter, $filter, ACTIVITY_DOC_ID, moment, $log){
        var self = this;

        self.getActivity = function(){
            $log.log("ActivityService.getActivity");

            return repository.getSafe(ACTIVITY_DOC_ID, self._createActivity)
                .then(function(activity){
                    return (!activity.inProgress) ? self._setFirstAvailableExercise(activity) : activity;
                });
        };

        self.getTotalExercisesForActivity = function(){
            $log.log("ActivityService.getTotalExercisesForActivity");

            var summaryExercises = [];

            return exerciseService.getExerciseSummary()
                .then(function(summary){
                    summaryExercises = summary.exercises;
                })
                .then(self.getActivity)
                .then(function(activity){
                    if (activity.currentExercise && summaryExercises.indexOf(activity.currentExercise) < 0){
                        summaryExercises.push(activity.currentExercise);
                    }

                    var i;
                    for (i = 0; i < activity.completedExercises.length; i++){
                        var exercise = activity.completedExercises[i];

                        if (summaryExercises.indexOf(exercise._id) < 0){
                            summaryExercises.push(exercise._id);
                        }
                    }

                    return { total: summaryExercises.length, activity: activity };
                });
        };

        self.startActivity = function(){
            $log.log("ActivityService.startActivity");

            return self.getActivity()
                .then(function(activity){
                    if (activity.suspended){
                        return self.resumeActivity();
                    }

                    if (activity.inProgress){
                        return activity;
                    }

                    if (activity.currentExercise){
                        activity.inProgress = true;
                        activity.started = true;
                        activity.lastStartTime = moment().unix();
                        return activity;
                    }

                    return $q.reject("no exercise to start");
                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                });
        };

        self.stopActivity = function(){
            $log.log("ActivityService.stopActivity");

            return self.getActivity()
                .then(function(activity){
                    if (!activity.inProgress){
                        return $q.reject("activity is not started");
                    }

                    if (!activity.currentExercise){
                        return $q.reject("no current exercise set");
                    }

                    var savedExercise = filterFilter(activity.completedExercises, {_id: activity.currentExercise});

                    if (savedExercise.length > 0){
                        return $q.reject("current exercise is already completed");
                    }

                    activity.inProgress = false;

                    if (!activity.suspended){
                        activity.duration = moment().unix() - activity.lastStartTime + activity.duration;
                    }

                    activity.completedExercises.push({_id: activity.currentExercise, duration: activity.duration});
                    activity.currentExercise = null;
                    activity.lastStartTime = 0;
                    activity.duration = 0;
                    activity.suspended = false;

                    return activity;

                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                });
        };

        self.suspendActivity = function(autostart){
            $log.log("ActivityService.suspendActivity");

            return self.getActivity()
                .then(function(activity){
                    if (!activity.inProgress){
                        return $q.reject("activity is not started");
                    }

                    if (!activity.currentExercise){
                        return $q.reject("no current exercise set");
                    }

                    if (activity.suspended){
                        return $q.reject("activity is already suspended");
                    }

                    activity.autostart = !!autostart;
                    activity.suspended = true;

                    if (!activity.autostart){
                        activity.duration = moment().unix() - activity.lastStartTime + activity.duration;
                        activity.lastStartTime = 0;
                    }

                    return activity;

                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                });
        };

        self.resumeActivity = function(){
            $log.log("ActivityService.resumeActivity");

            return self.getActivity()
                .then(function(activity){
                    if (!activity.inProgress){
                        return $q.reject("activity is not started");
                    }

                    if (!activity.currentExercise){
                        return $q.reject("no current exercise set");
                    }

                    if (!activity.suspended){
                        return $q.reject("activity is not suspended");
                    }

                    activity.suspended = false;

                    if (activity.autostart){
                        activity.duration = moment().unix() - activity.lastStartTime + activity.duration;
                    }

                    activity.lastStartTime = moment().unix();
                    activity.autostart = false;

                    return activity;

                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                });
        };

        self.revertActivity = function(){
            $log.log("ActivityService.revertActivity");

            return self.getActivity()
                .then(function(activity){
                    if (activity.inProgress){
                        return $q.reject("activity is already started");
                    }

                    if (activity.completedExercises.length == 0){
                        return $q.reject("no completed exercises");
                    }

                    var last = activity.completedExercises.pop();

                    activity.inProgress = false;
                    activity.duration = last.duration;
                    activity.currentExercise = last._id;
                    activity.lastStartTime = 0;

                    return activity;
                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                });
        };

        self.rateLastActivity = function(rating1, rating2){
            $log.log("ActivityService.rateLastActivity");

            if (rating1 < 0 || rating1 > 4){
                return $q.reject("invalid rating1");
            }

            if (rating2 < 0 || rating2 > 2){
                return $q.reject("invalid rating2");
            }

            return self.getActivity()
                .then(function(activity){
                    if (activity.completedExercises.length == 0){
                        return $q.reject("no completed exercises");
                    }

                    var last = activity.completedExercises[activity.completedExercises.length - 1];

                    if (last.rating){
                        return $q.reject("exercise is already rated");
                    }

                    last.rating = {rating1: rating1, rating2: rating2};
                    return activity;
                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                })
        };

        self.removeActivity = function(){
            return repository.remove(ACTIVITY_DOC_ID);
        }

        self._createActivity = function(){
            return {
                _id: ACTIVITY_DOC_ID,
                type: "activity",
                started: false,
                inProgress: false,
                suspended: false,
                autostart: false,
                lastStartTime: 0,
                duration: 0,
                currentExercise: null,
                completedExercises: []
            };
        };

        self._setFirstAvailableExercise = function(activity){
            return exerciseService.getExerciseSummary()
                .then(function(summary){
                    var i;

                    for (i = 0; i < summary.exercises.length; i++){
                        var exercise = summary.exercises[i];
                        var item = filterFilter(activity.completedExercises, {_id: exercise });

                        if (item.length == 0){
                            activity.currentExercise = exercise;
                            break;
                        }
                    }

                    return activity;
                })
        }
    }]);