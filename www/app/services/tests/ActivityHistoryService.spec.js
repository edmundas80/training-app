describe('ActivityHistoryService', function() {
    beforeEach(function() {
        module(function($provide){
            $provide.constant("DB_NAME", "TESTDB");
            $provide.constant("moment", moment);
        });

        module('pouchdb');
        module('training-app.services');
    });

    describe('saveActivityHistory', function(){
        it('when activity not set', function(done) {
            inject(function (pouchDB, ActivityHistoryService, Repository, $rootScope, $q) {
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                var activity = ActivityHistoryService.saveActivityHistory(null)
                    .catch(function(error){
                        expect(error).toBe("no activity set");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when no completed exercises', function(done) {
            inject(function (pouchDB, ActivityHistoryService, Repository, $rootScope, $q) {
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                var activity = ActivityHistoryService.saveActivityHistory({ completedExercises: []})
                    .catch(function(error){
                        expect(error).toBe("no completed exercises");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when success', function(done) {
            inject(function (pouchDB, ActivityHistoryService, ActivityService, Repository, $rootScope, $q) {
                var activity = ActivityService._createActivity();
                    activity.currentExercise = "1";
                activity.inProgress = false;
                activity.started = true;
                activity.lastStartTime = moment().unix() - 1;
                activity.completedExercises = [{_id: "1", duration: 10}, {_id: "2", duration: 20}];

                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityHistoryService.saveActivityHistory(activity)
                    .then(function(history){
                        expect(history.completedExercises.length).toBe(2);
                        expect(history.completedExercises[0]._id).toBe("1");
                        expect(history.completedExercises[0].duration).toBe(10);
                        expect(history.duration).toBe(30);
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(history);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });

    describe('getList', function(){
        it('when first page', function(done) {
            inject(function (pouchDB, ActivityHistoryService, Repository, $q, $rootScope) {
                spyOn(Repository, "all").and.returnValue($q.when({docs: [], lastId: null}));

                ActivityHistoryService.getList()
                    .then(function (result) {
                        expect(Repository.all.calls.argsFor(0)[0]).toEqual({startKey: "activityHistory_9999999999", pageSize: 10, descending: true});
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when next page', function(done) {
            inject(function (pouchDB, ActivityHistoryService, Repository, $q, $rootScope) {
                spyOn(Repository, "all").and.returnValue($q.when({docs: [], lastId: null}));

                ActivityHistoryService.getList("activityHistory_10")
                    .then(function (result) {
                        expect(Repository.all.calls.argsFor(0)[0]).toEqual({startKey: "activityHistory_10", pageSize: 10, descending: true, skip: 1});
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('ensure right document type and field mappings', function(done) {
            inject(function (pouchDB, ActivityHistoryService, Repository, $q, $rootScope, moment) {
                var date = moment().unix();
                spyOn(Repository, "all").and.returnValue($q.when({docs: [{_id: "activityHistory_1", type: "activityHistory", duration: 10, exerciseCount: 2, historyDate: date}, {_id: "1", type: "other"}], lastId: "activityHistory_1"}));

                ActivityHistoryService.getList()
                    .then(function (result) {
                        expect(result.docs.length).toBe(1);
                        expect(result.docs[0].type).toBe("activityHistory");
                        expect(result.docs[0].exerciseCount).toBe(2);
                        expect(result.docs[0].duration).toBe(10);
                        expect(result.docs[0].historyDate).toBe(date);
                        expect(result.docs[0]._id).toBe("activityHistory_1");
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });

    describe('get', function() {
        it('when id not set', function (done) {
            inject(function (pouchDB, ActivityHistoryService, Repository, $rootScope, $q) {
                spyOn(Repository, "get").and.returnValue($q.when());

                var activity = ActivityHistoryService.get(null)
                    .catch(function (error) {
                        expect(error).toBe("no id set");
                        expect(Repository.get).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when success', function (done) {
            inject(function (pouchDB, ActivityHistoryService, Repository, $rootScope, $q) {
                spyOn(Repository, "get").and.returnValue($q.when());

                var activity = ActivityHistoryService.get("1")
                    .then(function(history){
                        expect(Repository.get).toHaveBeenCalled();
                    })
                    .catch(function (error) {
                        fail(error);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });

});