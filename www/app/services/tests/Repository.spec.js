describe('Repository', function() {
    beforeEach(function() {
        module(function($provide){
           $provide.constant("DB_NAME", "TESTDB");
        });

        module('pouchdb');
        module('training-app.services');
    });

    it('allDocs with startkey', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "allDocs").and.returnValue($q.when({rows: []}));

            Repository.all({startKey: 1})
                .then(function (result) {
                    expect(Repository.db.allDocs.calls.argsFor(0)[0].startkey).toBe(1);
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('allDocs with endkey', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "allDocs").and.returnValue($q.when({rows: []}));

            Repository.all({endKey: 2})
                .then(function (result) {
                    expect(Repository.db.allDocs.calls.argsFor(0)[0].endkey).toBe(2);
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('allDocs with limit', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "allDocs").and.returnValue($q.when({rows: []}));

            Repository.all({pageSize: 20})
                .then(function (result) {
                    expect(Repository.db.allDocs.calls.argsFor(0)[0].limit).toBe(20);
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('allDocs with skip', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "allDocs").and.returnValue($q.when({rows: []}));

            Repository.all({skip: 10})
                .then(function (result) {
                    expect(Repository.db.allDocs.calls.argsFor(0)[0].skip).toBe(10);
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('allDocs with descending', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "allDocs").and.returnValue($q.when({rows: []}));

            Repository.all({descending: true})
                .then(function (result) {
                    expect(Repository.db.allDocs.calls.argsFor(0)[0].descending).toBe(true);
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('allDocs with 2 docs', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "allDocs").and.returnValue($q.when({rows: [{doc: {_id: 1}}, {doc: {_id: 2}}]}));

            Repository.all()
                .then(function (result) {
                    var docs = result.docs;
                    expect(result.lastId).toBe(2);
                    expect(docs.length).toBe(2);
                    expect(docs[0]._id).toBe(1);
                    expect(docs[1]._id).toBe(2);
                    expect(Repository.db.allDocs.calls.argsFor(0)).toEqual([{include_docs: true}]);
                    expect(Repository.db.allDocs).toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('allDocs with 0 docs', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "allDocs").and.returnValue($q.when({rows: []}));

            Repository.all()
                .then(function (result) {
                    var docs = result.docs;
                    expect(result.lastId).toBeNull();
                    expect(docs.length).toBe(0);
                    expect(Repository.db.allDocs.calls.argsFor(0)).toEqual([{include_docs: true}]);
                    expect(Repository.db.allDocs).toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('get with existing doc', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get").and.returnValue($q.when({_id: 1}));

            Repository.get("1")
                .then(function (doc) {
                    expect(doc).toBeTruthy();
                    expect(doc._id).toBe(1);
                    expect(Repository.db.get).toHaveBeenCalledWith("1");
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('get with non existing doc', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get").and.returnValue($q.reject({message: "missing"}));

            Repository.get("1")
                .catch(function(error){
                    expect(Repository.db.get).toHaveBeenCalledWith("1");
                    expect(error.message).toBe("missing");
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('get with no id', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get");

            Repository.get()
                .catch(function(error){
                    expect(Repository.db.get).not.toHaveBeenCalled();
                    expect(error).toBe("id is not specified");
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('exists with existing doc', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get").and.returnValue($q.when({_id: 1}));

            Repository.exists("1")
                .then(function (result) {
                    expect(result).toBeTruthy();
                    expect(Repository.db.get).toHaveBeenCalledWith("1");
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('exists with non existing doc', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get").and.returnValue($q.reject({error: {}, message: "missing"}));

            Repository.exists("1")
                .catch(function(error){
                    expect(Repository.db.get).toHaveBeenCalledWith("1");
                    expect(error.message).toBe("missing");
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('exists with no id', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get");

            Repository.exists()
                .catch(function(error){
                    expect(Repository.db.get).not.toHaveBeenCalled();
                    expect(error).toBe("id is not specified");
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('getSafe with existing doc', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            var self = this;

            spyOn(Repository.db, "get").and.returnValue($q.when({_id: 1}));

            self.createFn = function() {};
            spyOn(self, "createFn");

            Repository.getSafe("1", self.createFn)
                .then(function (doc) {
                    expect(doc).toBeTruthy();
                    expect(doc._id).toBe(1);
                    expect(Repository.db.get).toHaveBeenCalledWith("1");
                    expect(self.createFn).not.toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('getSafe with non existing doc', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            var self = this;

            spyOn(Repository.db, "get").and.returnValue($q.reject({error: {}, message: "missing"}));

            self.createFn = function() {};
            spyOn(self, "createFn").and.returnValue({_id: 2});

            Repository.getSafe("1", self.createFn)
                .then(function (doc) {
                    expect(doc).toBeTruthy();
                    expect(doc._id).toBe(2);
                    expect(Repository.db.get).toHaveBeenCalledWith("1");
                    expect(self.createFn).toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('getSafe with no id', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get");

            Repository.getSafe()
                .catch(function(error){
                    expect(Repository.db.get).not.toHaveBeenCalled();
                    expect(error).toBe("id is not specified");
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('getSafe with no createDocFn', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get");

            Repository.getSafe("1")
                .catch(function(error){
                    expect(Repository.db.get).not.toHaveBeenCalled();
                    expect(error).toBe("createDocFn is not specified");
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('saveOrUpdate with invalid document', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get");
            spyOn(Repository.db, "put");

            Repository.saveOrUpdate({})
                .catch(function(error){
                    expect(Repository.db.get).not.toHaveBeenCalled();
                    expect(Repository.db.put).not.toHaveBeenCalled();
                    expect(error).toBe("No document with _id");
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('saveOrUpdate with existing document', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get").and.returnValue($q.when({_id: 1, _rev: "aa"}));
            spyOn(Repository.db, "put");

            Repository.saveOrUpdate({_id: "1"})
                .then(function(){
                    expect(Repository.db.get).toHaveBeenCalledWith("1");
                    expect(Repository.db.put).toHaveBeenCalled();
                    expect(Repository.db.put.calls.argsFor(0)[0]._id).toBe("1");
                    expect(Repository.db.put.calls.argsFor(0)[0]._rev).toBe("aa");
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('saveOrUpdate with new document', function(done) {
        inject(function (pouchDB, Repository, $q, $rootScope) {
            spyOn(Repository.db, "get").and.returnValue($q.reject({error: {}, message: "missing"}));
            spyOn(Repository.db, "put");

            Repository.saveOrUpdate({_id: "1"})
                .then(function(){
                    expect(Repository.db.get).toHaveBeenCalledWith("1");
                    expect(Repository.db.put).toHaveBeenCalledWith({_id: "1"});
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });
});