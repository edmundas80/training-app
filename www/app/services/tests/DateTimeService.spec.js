describe('DateTimeService', function() {
    beforeEach(function() {
        module(function($provide){
            $provide.constant("DB_NAME", "TESTDB");
            $provide.constant("moment", moment);
            $provide.constant("locale", "lt");
        });

        module('training-app.services');
    });

    describe('toLongDate', function(){
        it('when success', function(done) {
            inject(function (DateTimeService, $rootScope) {
                try{
                    expect(DateTimeService.toLongDate(1499831096).indexOf("2017 m. liepos 12 d., ") != -1).toBe(true);
                }
                finally{
                    done();
                    $rootScope.$digest();
                }
            })
        });
    });

    describe('toDuration', function(){
        it('when success', function(done) {
            inject(function (DateTimeService, $rootScope) {
                try{
                    expect(DateTimeService.toDuration(1000)).toBe("17 minučių");
                }
                finally{
                    done();
                    $rootScope.$digest();
                }
            })
        });
    });
});