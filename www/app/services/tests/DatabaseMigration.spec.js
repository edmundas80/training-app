describe('DatabaseMigration', function() {
    beforeEach(function() {
        module(function($provide){
           $provide.constant("DB_NAME", "TESTDB");
        });

        module('pouchdb');
        module('training-app.services');
    });

    it('executeDbMigration first time', function(done) {
        inject(function (pouchDB, DatabaseMigration, Repository, $q, $rootScope, $http) {
            spyOn($http, "get").and.returnValue($q.when({data: []}));
            spyOn(Repository.db, "get").and.returnValue($q.reject({error: {}, message: "missing"}));
            spyOn(DatabaseMigration, "_updateDb").and.returnValue($q.when({}));
            spyOn(Repository.db, "allDocs");
            spyOn(Repository.db, "remove");
            spyOn(Repository.db, "bulkDocs");

            DatabaseMigration.executeDbMigration()
                .then(function () {
                    expect(DatabaseMigration._updateDb).toHaveBeenCalledWith(0);
                    expect(Repository.db.allDocs).not.toHaveBeenCalled();
                    expect(Repository.db.remove).not.toHaveBeenCalled();
                    expect(Repository.db.bulkDocs).not.toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('executeDbMigration no docs', function(done) {
        inject(function (pouchDB, DatabaseMigration, Repository, $q, $rootScope, $http) {
            spyOn($http, "get").and.returnValue($q.when({data: []}));
            spyOn(Repository.db, "get").and.returnValue($q.when({}));
            spyOn(Repository.db, "allDocs");
            spyOn(Repository.db, "remove");
            spyOn(Repository.db, "bulkDocs");

            DatabaseMigration.executeDbMigration()
                .then(function () {
                    expect($http.get).toHaveBeenCalled();
                    expect(Repository.db.allDocs).not.toHaveBeenCalled();
                    expect(Repository.db.remove).not.toHaveBeenCalled();
                    expect(Repository.db.bulkDocs).not.toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('executeDbMigration no versionId document', function(done) {
        inject(function (pouchDB, DatabaseMigration, Repository, $q, $rootScope, $http) {
            spyOn($http, "get").and.returnValue($q.when({data: [{_id: "1"}]}));
            spyOn(Repository.db, "get").and.returnValue($q.when({}));
            spyOn(Repository.db, "allDocs");
            spyOn(Repository.db, "remove");
            spyOn(Repository.db, "bulkDocs");

            DatabaseMigration.executeDbMigration()
                .then(function () {
                    expect($http.get).toHaveBeenCalled();
                    expect(Repository.db.allDocs).not.toHaveBeenCalled();
                    expect(Repository.db.remove).not.toHaveBeenCalled();
                    expect(Repository.db.bulkDocs).not.toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('executeDbMigration same db version', function(done) {
        inject(function (pouchDB, DatabaseMigration, Repository, $q, $rootScope, $http, DB_VERSION_DOC_ID) {
            spyOn($http, "get").and.returnValue($q.when({data: [{_id: DB_VERSION_DOC_ID, version: 0}, {_id: "1"}]}));
            spyOn(Repository.db, "get").and.returnValue($q.when({}));
            spyOn(Repository.db, "allDocs").and.returnValue($q.when({rows: []}));
            spyOn(Repository.db, "remove");
            spyOn(Repository.db, "bulkDocs");

            DatabaseMigration.executeDbMigration()
                .then(function () {
                    expect($http.get).toHaveBeenCalled();
                    expect(Repository.db.allDocs).not.toHaveBeenCalled();
                    expect(Repository.db.remove).not.toHaveBeenCalled();
                    expect(Repository.db.bulkDocs).not.toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('executeDbMigration newer db version', function(done) {
        inject(function (pouchDB, DatabaseMigration, Repository, $q, $rootScope, $http, DB_VERSION_DOC_ID) {
            spyOn($http, "get").and.returnValue($q.when({data: [{_id: DB_VERSION_DOC_ID, version: 1}, {_id: "1"}]}));
            spyOn(Repository.db, "get").and.returnValue($q.when({}));
            spyOn(Repository.db, "allDocs").and.returnValue($q.when({rows: [
                {id: DB_VERSION_DOC_ID, error: false, value: {deleted: false, rev: "AA"}},
                {id: "1", error: false, value: {deleted: false, rev: "BB"}}
            ]}));
            spyOn(Repository.db, "remove").and.returnValue($q.when({}));
            spyOn(Repository.db, "bulkDocs");

            DatabaseMigration.executeDbMigration()
                .then(function () {
                    expect($http.get).toHaveBeenCalled();
                    expect(Repository.db.allDocs).toHaveBeenCalled();
                    expect(Repository.db.remove).toHaveBeenCalledTimes(2);
                    expect(Repository.db.bulkDocs).toHaveBeenCalledWith([{_id: DB_VERSION_DOC_ID, version: 1}, {_id: "1"}]);
                })
                .catch(function(error){
                    fail(error);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });
});