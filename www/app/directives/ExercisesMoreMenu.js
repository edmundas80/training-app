angular.module('training-app.directives')
    .directive('exercisesMoreMenu', function($ionicPopover) {
        return {
            restrict: 'E',
            replace: true,
            template: '<a href="" class="button button-icon icon ion-android-more-vertical"></a>',
            scope: {
                onDiscard: '&'
            },
            link: function(scope, element){
                var template =
                    '<ion-popover-view class="fit">'+
                    '   <ion-content>'+
                    '       <div class="list">'+
                    '           <a class="item" href="" ng-click="discard()">{{"DISCARD" | translate}}</a>'+
                    '        </div>'+
                    '   </ion-content>'+
                    '</ion-popover-view>';

                var popover = $ionicPopover.fromTemplate(template, { scope: scope });

                element.on('click', function(event) {popover.show(event);});

                scope.discard = function(){
                    scope.onDiscard();
                    popover.hide();
                };

                scope.$on('$destroy', function() {
                    popover.remove();
                });
            }
        };
    });