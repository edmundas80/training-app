angular.module('training-app.directives')
    .directive('closeButton', function() {
        return {
            restrict: 'E',
            replace: true,
            template: '<a href="" class="button button-icon"></a>',
            link: function(scope, element){
                if (ionic.Platform.isAndroid()){
                    element.addClass("ion-android-close");
                }
                else {
                    element.addClass("ion-ios-close-empty");
                }
            }
        };
    });